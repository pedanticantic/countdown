<?php

class Result
{
    /**
     * @var int|null
     */
    private $bestAnswer;

    /**
     * @var string
     */
    private $bestSolution;

    /**
     * @var array
     */
    private $correctSolutions;

    public function __construct()
    {
        $this->correctSolutions = [];
    }

    public function hasBestAnswer()
    {
        return !is_null($this->bestAnswer);
    }

    public function getBestAnswer()
    {
        return $this->bestAnswer;
    }

    public function setBestAnswer($answer, $solution)
    {
        $this->bestAnswer = $answer;
        $this->bestSolution = $solution;
    }

    public function getBestSolutionAsString()
    {
        return $this->bestSolution;
    }

    public function bestAnswerMatchesTarget($target)
    {
        return $this->hasBestAnswer() && $this->bestAnswer === $target;
    }

    public function getMissedBy($target)
    {
        return abs($this->getBestAnswer() - $target);
    }

    public function addCorrectSolution($solution)
    {
        $this->correctSolutions[] = $solution;
    }

    public function getAllCorrect()
    {
        return array_unique($this->correctSolutions);
    }
}
