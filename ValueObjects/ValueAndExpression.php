<?php

/**
 * This represents a value and the expression that generated it. The expression could simply be the same as the number,
 * or it could be an actual expression.
 */
class ValueAndExpression
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var string
     */
    private $expression;

    public function __construct($value, $expression)
    {
        $this->value = $value;
        $this->expression = $expression;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getExpression()
    {
        return $this->expression;
    }

}
