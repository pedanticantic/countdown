<?php

/**
 * This represents a set or list or collection of value-expression pairs.
 * For now, we'll just use an array of instances of ValueAndExpression.
 */
class ValueAndExpressionSet
{
    /**
     * @var ValueAndExpression[]
     */
    private $valuesAndExpressions;

    /**
     * ValueAndExpressionSet constructor.
     * @param ValueAndExpression[] $valuesAndExpressions
     */
    public function __construct(array $valuesAndExpressions = [])
    {
        $this->valuesAndExpressions = $valuesAndExpressions;
    }

    public static function buildFromValues(array $initialValues)
    {
        $valuesAndExpressions = [];
        foreach ($initialValues as $initialValue) {
            $valuesAndExpressions[] = new ValueAndExpression((int) $initialValue, $initialValue);
        }

        return new self($valuesAndExpressions);
    }

    public function append(ValueAndExpression $valueAndExpression)
    {
        $this->valuesAndExpressions[] = $valueAndExpression;
    }

    public function getCount()
    {
        return count($this->valuesAndExpressions);
    }

    public function getValueAt($index)
    {
        if ($index < 0 || $index > $this->getCount()) {
            throw new RuntimeException('Index out of range for valueAt');
        }

        return $this->valuesAndExpressions[$index]->getValue();
    }

    public function getExpressionAt($index)
    {
        if ($index < 0 || $index > $this->getCount()) {
            throw new RuntimeException('Index out of range for expressionAt');
        }

        return $this->valuesAndExpressions[$index]->getExpression();
    }
}
